import React, { Component } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Advertising from './components/Advertising/Advertising';
import Games from './components/Games/Games';
import Hobby from './components/Hobby/Hobby';
import Politics from './components/Politics/Politics';
import Sport from './components/Sport/Sport';

class App extends Component {
  state = {
    activeNews: <Advertising />,
  };

  showNews(category) {
    this.setState({
      activeNews: category,
    });
  }

  render() {
    return (
      <div className="news">
        <div className="news__nav">
          <div className="news__wrapper">
            <h2 className="news__text">Advertising</h2>
            <Button
              showNews={() => {
                this.showNews(<Advertising />);
              }}
            />
          </div>
          <div className="news__wrapper">
            <h2 className="news__text">Games</h2>
            <Button
              showNews={() => {
                this.showNews(<Games />);
              }}
            />
          </div>
          <div className="news__wrapper">
            <h2 className="news__text">Hobby</h2>
            <Button
              showNews={() => {
                this.showNews(<Hobby />);
              }}
            />
          </div>
          <div className="news__wrapper">
            <h2 className="news__text">Politics</h2>
            <Button
              showNews={() => {
                this.showNews(<Politics />);
              }}
            />
          </div>
          <div className="news__wrapper">
            <h2 className="news__text">Sport</h2>
            <Button
              showNews={() => {
                this.showNews(<Sport />);
              }}
            />
          </div>
        </div>
        <div className="news__list">{this.state.activeNews}</div>
      </div>
    );
  }
}

export default App;
