import React from 'react';
import NewsItem from '../News/NewsItem';
import newsData from '../newsData';

const Advertising = () => {
  const categoryNews = newsData
    .filter((item) => item.category === 'Advertising')
    .map((item) => {
      return <NewsItem key={item.id} title={item.title} date={item.date} />;
    });

  return <div className="news-advertising">{categoryNews}</div>;
};

export default Advertising;
