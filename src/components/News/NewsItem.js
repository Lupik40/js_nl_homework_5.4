import React from 'react';
import './News.scss';

const NewsItem = (props) => {
  return (
    <div className="news__item">
      <h3 className="news__title">{props.title}</h3>
      <p className="news__date">{props.date}</p>
    </div>
  );
};

export default NewsItem;
