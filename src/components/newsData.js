const newsData = [
  {
    id: 1,
    title: 'Advertising title #1',
    date: '17.12.2021',
    category: 'Advertising',
  },
  {
    id: 2,
    title: 'Advertising title #2',
    date: '02.03.2020',
    category: 'Advertising',
  },
  {
    id: 3,
    title: 'Advertising title #3',
    date: '15.06.2022',
    category: 'Advertising',
  },
  {
    id: 4,
    title: 'Games title #1',
    date: '09.05.2019',
    category: 'Games',
  },
  {
    id: 5,
    title: 'Games title #2',
    date: '01.09.2020',
    category: 'Games',
  },
  {
    id: 6,
    title: 'Games title #3',
    date: '23.02.2021',
    category: 'Games',
  },
  {
    id: 7,
    title: 'Hobby title #1',
    date: '28.04.2018',
    category: 'Hobby',
  },
  {
    id: 8,
    title: 'Hobby title #2',
    date: '19.01.2022',
    category: 'Hobby',
  },
  {
    id: 9,
    title: 'Hobby title #3',
    date: '25.06.2020',
    category: 'Hobby',
  },
  {
    id: 10,
    title: 'Politics title #1',
    date: '07.11.2019',
    category: 'Politics',
  },
  {
    id: 11,
    title: 'Politics title #2',
    date: '01.02.2022',
    category: 'Politics',
  },
  {
    id: 12,
    title: 'Politics title #3',
    date: '19.11.2019',
    category: 'Politics',
  },
  {
    id: 13,
    title: 'Sport title #1',
    date: '29.09.2020',
    category: 'Sport',
  },
  {
    id: 14,
    title: 'Sport title #2',
    date: '16.01.2018',
    category: 'Sport',
  },
  {
    id: 15,
    title: 'Sport title #3',
    date: '01.08.2021',
    category: 'Sport',
  },
];

export default newsData;
