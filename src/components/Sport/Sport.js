import React from 'react';
import NewsItem from '../News/NewsItem';
import newsData from '../newsData';

const Sport = () => {
  const categoryNews = newsData
    .filter((item) => item.category === 'Sport')
    .map((item) => {
      return <NewsItem key={item.id} title={item.title} date={item.date} />;
    });

  return <div className="news-sport">{categoryNews}</div>;
};

export default Sport;
