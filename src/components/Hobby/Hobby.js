import React from 'react';
import NewsItem from '../News/NewsItem';
import newsData from '../newsData';

const Hobby = () => {
  const categoryNews = newsData
    .filter((item) => item.category === 'Hobby')
    .map((item) => {
      return <NewsItem key={item.id} title={item.title} date={item.date} />;
    });

  return <div className="news-hobby">{categoryNews}</div>;
};

export default Hobby;
