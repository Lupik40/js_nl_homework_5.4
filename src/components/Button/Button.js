import React from 'react';
import './Button.scss';

const Button = (props) => {
  return (
    <button className="news__btn" onClick={props.showNews}>
      Show news
    </button>
  );
};

export default Button;
