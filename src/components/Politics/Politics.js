import React from 'react';
import NewsItem from '../News/NewsItem';
import newsData from '../newsData';

const Politics = () => {
  const categoryNews = newsData
    .filter((item) => item.category === 'Politics')
    .map((item) => {
      return <NewsItem key={item.id} title={item.title} date={item.date} />;
    });

  return <div className="news-politics">{categoryNews}</div>;
};

export default Politics;
